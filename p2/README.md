
# LIS3781 Advanced Database Management

## Sarah Huerta

### Project 2 Requirements:

    * Installation for Mongodb and importation
    * Introduction to NoSQL DBMS and MongoDB
    * Required reports and JSON Code Solution

| MongoDB Shell Commands | Required Reports with JSON Solutions |
| -- | -- |
| ![shell](shell.png) | ![json](json.png) |
